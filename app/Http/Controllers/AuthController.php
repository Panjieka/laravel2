<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function table(){
        return view('layout.table');
    }
    public function datatables(){
        return view('layout.datatables');
    }
}
